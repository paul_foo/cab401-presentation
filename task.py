import boto.sqs
import boto
from boto.s3.key import Key
from boto.sqs.message import Message
import logging
import json
import time


logging.basicConfig(level=logging.INFO)


def get_queue():
    conn = boto.sqs.connect_to_region(
        region_name='us-east-2',
    )

    queue = conn.get_queue('cab401')
    return queue


def pull_message(q):
    rs = q.get_messages()

    if rs:
        m = rs[0]
        return m
    else:
        return None


def process_message(m, inst_id, b):
    body_dict = json.loads(m.get_body())
    task_id = body_dict.get("id")
    logging.info("received task with id: {}".format(body_dict.get("id")))
    logging.info("processing task")
    key_name = "{}: {}".format(inst_id, task_id)
    save_output(key_name, "Hello World", b)
    logging.info("Hello World!")
    logging.info("task {} complete".format(body_dict.get("id")))


def delete_message(q, m):
    q.delete_message(m)
    logging.info("deleting task from queue")


def get_bucket():
    conn = boto.s3.connect_to_region(region_name='us-east-2')
    b = conn.get_bucket('cab401')
    return b


def save_output(key_name, output, bucket):
    k = Key(bucket)
    k.key = key_name
    k.set_contents_from_string(output)


def populate_queue(num_messsages=100):
    q = get_queue()

    for i in range(num_messsages):
        m = Message()
        body = json.dumps({'id': i})
        m.set_body(body)
        q.write(m)


def run(id):
    q = get_queue()
    b = get_bucket()
    while True:
        logging.info("checking queue for tasks...")
        m = pull_message(q)
        if m:
            process_message(m, id, b)
            delete_message(q, m)
        time.sleep(15)
